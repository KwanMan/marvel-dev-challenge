# marvel-dev-challenge

To run:

```sh
  # bootstrap dependencies
  $ yarn install

  # provide personal access token
  $ ACCESS_TOKEN=super-secret-personal-access-token yarn start

  # open in your favourite browser
  $ open http://localhost:1234
```
