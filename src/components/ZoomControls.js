import React from 'react'
import classnames from 'classnames'

import './ZoomControls.css'

export default function ZoomControls (props) {
  const { zoom, onIncreaseZoom, onDecreaseZoom, onFitZoom } = props

  const className = classnames('ZoomControls', props.className)

  return (
    <div className={className}>
      <span className='ZoomControls-decrease' onClick={onDecreaseZoom}>
        -
      </span>
      <span className='ZoomControls-percentage' onClick={onFitZoom}>
        {zoom}%
      </span>
      <span className='ZoomControls-increase' onClick={onIncreaseZoom}>
        +
      </span>
    </div>
  )
}
