import React from 'react'
import ZoomControls from './ZoomControls'

import './ScreenViewer.css'

const MIN_ZOOM = 50
const MAX_ZOOM = 100
const ZOOM_INCREMENT = 5

export default class ScreenViewer extends React.Component {
  constructor (props) {
    super(props)
    this.viewerRef = React.createRef()
    this.state = {
      zoom: false
    }
  }
  componentDidMount () {
    this.handleFitZoom()
  }
  render () {
    const { zoom } = this.state

    return (
      <div className='ScreenViewer'>
        <ZoomControls
          className='ScreenViewer-controls'
          zoom={zoom}
          onIncreaseZoom={() => this.handleIncreaseZoom()}
          onDecreaseZoom={() => this.handleDecreaseZoom()}
          onFitZoom={() => this.handleFitZoom()}
        />
        <div className='ScreenViewer-viewer' ref={this.viewerRef}>
          {this.renderScreen()}
        </div>
      </div>
    )
  }

  renderScreen () {
    const { zoom } = this.state
    const { url, width } = this.props

    if (zoom === false) {
      // we don't want to render on the first round, we need to wait
      // for the container to render first to get the dimensions
      return null
    }

    const imageWidth = Math.floor(width * (zoom / 100))

    return (
      <img
        className='ScreenViewer-screen'
        src={url}
        style={{ width: imageWidth }}
      />
    )
  }

  handleIncreaseZoom () {
    const { zoom } = this.state
    this.setState({ zoom: Math.min(zoom + ZOOM_INCREMENT, MAX_ZOOM) })
  }

  handleDecreaseZoom () {
    const { zoom } = this.state
    this.setState({ zoom: Math.max(zoom - ZOOM_INCREMENT, MIN_ZOOM) })
  }

  handleFitZoom () {
    this.setState({
      zoom: getFitZoom(
        this.viewerRef.current.getBoundingClientRect().width,
        this.props.width
      )
    })
  }
}

function getFitZoom (boundingWidth, imageWidth) {
  // get the perfect zoom
  let zoom = 100 * (boundingWidth / imageWidth)

  // make it a multiple of the increment
  zoom = Math.floor(zoom / ZOOM_INCREMENT) * ZOOM_INCREMENT

  // make sure it's within our range
  zoom = Math.min(zoom, MAX_ZOOM)
  zoom = Math.max(zoom, MIN_ZOOM)

  return zoom
}
