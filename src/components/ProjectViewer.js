import React from 'react'
import gql from 'graphql-tag'
import { get, omit, pick } from 'lodash-es'
import ReactSelect from 'react-select'
import ScreenViewer from './ScreenViewer'
import withQuery from '../utils/withQuery'

import './ProjectViewer.css'

const GET_PROJECTS = gql`
query ProjectScreens ($projectPk: Int!) {
  project(pk: $projectPk) {
    screens(first: 5) {
      edges {
        node {
          displayName
          content {
            ... on ImageScreen {
              url
              height
              width
            }
          }
        }
      }
    }
  }
}`

function mapVariables (props) {
  return pick(props, 'projectPk')
}

function mapData (data) {
  const edges = get(data, 'project.screens.edges') || []
  return {
    screens: edges.map(({ node }) => ({
      name: node.displayName,
      ...omit(node.content, '__typename')
    }))
  }
}

class ProjectViewer extends React.Component {
  constructor () {
    super()
    this.state = {
      selectedScreen: false
    }
  }
  componentWillReceiveProps (nextProps) {
    if (this.props.loading && !nextProps.loading && nextProps.data) {
      this.setState({
        selectedScreen: toOption(nextProps.data.screens[0])
      })
    }
  }
  render () {
    const { loading, error, data } = this.props
    if (loading) return 'Loading...'
    if (error) return `Error! ${error.message}`

    const { selectedScreen } = this.state

    return (
      <div className='ProjectViewer'>
        <ReactSelect
          className='ProjectViewer-selector'
          options={data.screens.map(toOption)}
          value={selectedScreen}
          onChange={screen => {
            this.setState({
              selectedScreen: screen
            })
          }}
        />
        <ScreenViewer key={selectedScreen.name} {...selectedScreen} />
      </div>
    )
  }
}

function toOption (screen) {
  return { ...screen, value: screen.name, label: screen.name }
}

export default withQuery(GET_PROJECTS, mapVariables, mapData)(ProjectViewer)
