import React from 'react'
import { Query } from 'react-apollo'
import { noop, identity } from 'lodash-es'

export default function withQuery (
  query,
  mapVariables = noop,
  mapData = identity
) {
  return function (Wrapped) {
    return function (props) {
      return (
        <Query query={query} variables={mapVariables(props)}>
          {({ loading, error, data }) => {
            const propsToPass = { loading, error }
            if (data) {
              try {
                propsToPass.data = mapData(data)
              } catch (e) {
                propsToPass.error = e
              }
            }
            return <Wrapped {...propsToPass} />
          }}
        </Query>
      )
    }
  }
}
