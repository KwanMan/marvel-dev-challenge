import React from 'react'
import ReactDOM from 'react-dom'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

import ProjectViewer from './components/ProjectViewer'

import './index.css'

// hardcoded for the purpose of this demo
const PROJECT_PK = 3274163

const client = new ApolloClient({
  uri: 'https://api.marvelapp.com/graphql/',
  headers: {
    Authorization: `Bearer ${process.env.ACCESS_TOKEN}`
  }
})

const App = () => (
  <ApolloProvider client={client}>
    <div className='App'>
      <ProjectViewer projectPk={PROJECT_PK} />
    </div>
  </ApolloProvider>
)

const mountNode = document.querySelector('#root')

ReactDOM.render(<App />, mountNode)
